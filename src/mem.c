#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  const size_t actual_size = region_actual_size(size_from_capacity((block_capacity) {query}).bytes);
  void* region_addr = map_pages(addr, actual_size, MAP_FIXED);
  bool extends = true;
    if(region_addr == MAP_FAILED){
          extends=false;
	  region_addr = map_pages(addr, actual_size , 0);
	  if(region_addr == MAP_FAILED)
		  return REGION_INVALID;
  }
  block_init(region_addr, (block_size){.bytes = actual_size}, NULL);
  return (struct region) {.addr = region_addr, .size = actual_size, .extends = extends};
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
  struct block_header* start_block = HEAP_START;
  struct block_header* curr_block = start_block;
  size_t size;
    while (curr_block) {
        start_block=curr_block;
        size = 0;
        while (block_after(curr_block) == curr_block->next) {
            size += size_from_capacity(curr_block->capacity).bytes;
            curr_block = curr_block->next;
        }
        size += size_from_capacity(curr_block->capacity).bytes;
        curr_block = curr_block->next;
        if(munmap(start_block, size)==-1){
          perror("Error in heap_term");
        };
    }

}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block || !block_splittable(block, query)) { return false; }
  query = size_max(query, BLOCK_MIN_CAPACITY);
  block_size first_block_size = size_from_capacity((block_capacity){query});
  block_size second_block_size = (block_size){size_from_capacity(block->capacity).bytes - first_block_size.bytes};
  void *second_block = (uint8_t *)block + first_block_size.bytes;
  block_init(second_block, second_block_size, block->next);
  block_init(block, first_block_size, second_block);
  return true;


}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (!block || !block -> next || !mergeable(block, block->next))  return false; 
  size_t merged_size = size_from_capacity(block->capacity).bytes + size_from_capacity(block->next->capacity).bytes;
  block_init(block, (block_size){merged_size}, block->next->next);
  return true;

}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if (block == NULL) {
        return (struct block_search_result) {.type=BSR_CORRUPTED, .block=NULL};
    }
  struct block_header* current = block;
  struct block_header* previous = NULL;
  while (current != NULL) {
        while (try_merge_with_next(current)) {}
        if (block_is_big_enough(sz, current)&&current->is_free) {
            return (struct block_search_result) {
                .block = current,
                .type = BSR_FOUND_GOOD_BLOCK
            };
        }
        previous = current;
        current = current->next;
    }
    return (struct block_search_result) {
        .block = previous,
        .type = BSR_REACHED_END_NOT_FOUND
    };

  
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }
  return result;

}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
   if (last == NULL)  return NULL; 
   struct region next_reg = alloc_region(block_after(last), query);
   last->next = next_reg.addr;
   if (region_is_invalid(&next_reg)){
        return NULL;
   }

    last->next = next_reg.addr;
    if (try_merge_with_next(last)){
        return last;
    }
    return next_reg.addr;

   
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
   if (heap_start == NULL || query <= 0) {
        return NULL;
    }
   size_t new_query = size_max(query, BLOCK_MIN_CAPACITY);
   struct block_search_result result = try_memalloc_existing(new_query, heap_start);
   if (result.type == BSR_CORRUPTED) {return NULL;}
   if (result.type == BSR_FOUND_GOOD_BLOCK) {return result.block;}
   struct block_header* new_block = grow_heap(result.block, query);
   if (!new_block) return NULL;
   result = try_memalloc_existing(new_query, new_block);
   return result.block;
  
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  try_merge_with_next(header);
}
